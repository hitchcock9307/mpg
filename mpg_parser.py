from sly import Lexer, Parser
from pprint import pprint

from mpg_lexer import MPGLexer

class MPGParser(Parser):
    # Get the token list from the lexer (required)
    tokens = MPGLexer.tokens

    # debugfile = 'parser.out'
    messages = {}
    def error(self, t):
        print("Unexpected token " + 
            t.type +
            " at line number " +
            str(t.lineno) +
            " character " +
            str(t.index) )

    @_('sections')
    def file(self, p):
        return p.sections

    @_('sections section')
    def sections(self, p):
        return p.sections + [ p.section ]

    @_('section')
    def sections(self, p):
        return [ p.section ]

    @_('message')
    def section(self, p):
        return p.message


    @_('preprocessor')
    def section(self, p):
        return p.preprocessor

    @_('PREPROC')
    def preprocessor(self, p):
        return { "type": "preprocessor",
                 "directive": p.PREPROC }


    @_('MESSAGE IDENTIFIER LCURL fields RCURL')
    def message(self, p):
        message = {}
        message["type"] = "message"
        message["name"] = p.IDENTIFIER
        message["fields"] = p.fields
        return message

    @_('fields field')
    def fields(self, p):
        return p.fields + [ p.field ]

    @_('field')
    def fields(self, p):
        return [ p.field ]

    @_('FIELD_TYPE IDENTIFIER SEMICOLON')
    def field(self, p):
        return {"type": p.FIELD_TYPE, "identifier": p.IDENTIFIER}
    
    @_('FIELD_TYPE IDENTIFIER LSQUARE constant RSQUARE SEMICOLON')
    def field(self, p):
        return {
            "type": "array",
            "array_type": p.FIELD_TYPE,
            "identifier": p.IDENTIFIER,
            "length": p.constant
        }

    # @_('expr OPERATOR factor')
    # def expr(self, p):
    #     return {
    #         'lexpr': p.expr0, 
    #         'operator': p.OPERATOR,
    #         'rexpr': p.expr1
    #     }
    
    # @_('factor')
    # def expr(self, p):
    #     return p.factor
    
    # @_('IDENTIFIER')
    # def factor(self, p):
    #     return {'expr': p.IDENTIFIER}

    # @_('CONST_IDENT')
    # def constant(self, p):
    #     return {'expr': p.CONST_IDENT}

    @_('number')
    def constant(self, p):
        return {'expr': p.number}
    
    # @_('number')
    # def factor(self, p):
    #     return {'expr': p.number}

    @_('HEX')
    def number(self, p):
        return int(p.HEX, 16)
    
    @_('DECIMAL')
    def number(self, p):
        return int(p.DECIMAL)

