from sly import Lexer, Parser
from pprint import pprint

class MPGLexer(Lexer):
    tokens = { 
        COMMENT,        CONST_IDENT,    MESSAGE,
        PARSER,         PRESENTATION,   ASSERTION,
        LPAREN,         RPAREN,         LCURL,
        RCURL,          FIELD_TYPE,     EQUALS,
        LSQUARE,        RSQUARE,        DECIMAL,
        SET,            SEMICOLON,      B_OR,
        B_AND,          IDENTIFIER,     COMPARATOR,
        OPERATOR,       PREPROC,        HEX,
        KEYWORD,
    }
    ignore = ' \t\n'

    @_(r'\s*//.*')
    def COMMENT(self, t):
        return

    MESSAGE = r'struct'
    LPAREN = r'\('
    RPAREN = r'\)'
    LCURL = r'\{'
    RCURL = r'\}'
    LSQUARE = r'\['
    RSQUARE = r'\]'
    FIELD_TYPE = r'u?int[0-9]+'
    COMPARATOR = r'(==|<=|>=|!=)'
    SET = r'='
    SEMICOLON = r'\;'
    B_OR = r'\|'
    B_AND = r'&'

    @_(r'\#.*')
    def PREPROC(self, t):
        print(t)
        return t

    OPERATOR = r'[+-/*]'
    HEX = r'(0x[0-9A-Fa-f]+|0X[0-9A-Fa-f])'
    DECIMAL = r'[0-9]+'


    @_(r'[A-Za-z0-9_]+')
    def IDENTIFIER(self, t):
        return t

    @_(r'\n+')
    def ignore_newline(self, t):
        self.lineno += t.value.count('\n')

    def error(self, t):
        print("Illegal character '%s'" % t.value[0])
        self.index += 1