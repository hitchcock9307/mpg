import sys

from sly import Lexer, Parser
from pprint import pprint

from mpg_lexer import MPGLexer
from mpg_parser import MPGParser
from mpg_handler import MPGHandler


if __name__ == '__main__':
    file = open(sys.argv[1])
    data = file.read()
    lexer = MPGLexer()
    parser = MPGParser()
    lexed = lexer.tokenize(data)
    parsed = parser.parse(lexed)
    
    handler = MPGHandler(parsed, sys.argv[2])

