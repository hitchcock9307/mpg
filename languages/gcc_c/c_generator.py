TYPE_MAP = {
    "uint8": "uint8_t",
    "uint16": "uint16_t",
    "uint32": "uint32_t",
    "uint64": "uint64_t",
    "int8": "int8_t",
    "int16": "int16_t",
    "int32": "int32_t",
    "int64": "int64_t",
}

TAB = "    "

class CGenerator():
    def __init__(self):
        pass

    def _struct_array_field(self, field):
        raise NotImplementedError("Have not yet implemented array field in the C generator")
        return []

        
    def _struct_fixed_field(self, field):
        return [f"{TAB}{TYPE_MAP[field['type']]} {field['identifier']};"]


    def _packed_struct(self, message):
        out = [f"struct __attribute__((__packed__)) packed_{message['name']} {{"]
        for field in message['fields']:
            if field['type'] == 'array':
                out.extend(self._struct_array_field(field))
            else:
                out.extend(self._struct_fixed_field(field))
        out.append("}")
        return out

    
    def _struct(self, message):
        out = [f"struct {message['name']} {{"]
        for field in message['fields']:
            if field['type'] == 'array':
                out.extend(self._struct_array_field(field))
            else:
                out.extend(self._struct_fixed_field(field))
        out.append("}")
        return out

    def generate_message(self, message):
        self.generate_header_file(message)
        self.generate_c_file(message)
        
    def generate_header_file(self, message):
        header_out = ["#include <stdint.h>"]
        header_out.extend(self._packed_struct(message))
        header_out.extend(self._struct(message))


        header_file = open(f"message_{message['name']}.h", 'w')
        header_file.write('\n'.join(header_out))
        header_file.close()

        
    def generate_c_file(self, message):
        c_out = [f"#include <stdint.h>",
                    f"#include \"message_{message['name']}.h\"",
                    "",
                    f"bool deserialize_{message['name']}(void* buf, size_t buf_len, {message['name']}* message){{"]

        for field in message['fields']:
            if field['type'] == 'array':
                c_out.append(f"{TAB}buf_len -= sizeof(message->{field['identifier']});")
                c_out.append(f"{TAB}if (buf_len > 0) {{")
                c_out.append(f"{TAB * 2}memcpy(buf, &(message->{field['identifier']}), sizeof(message->{field['identifier']}));")
                c_out.append(f"{TAB}}}")
            else:
                c_out.append(f"{TAB}buf_len -= sizeof(message->{field['identifier']});")
                c_out.append(f"{TAB}if (buf_len > 0) {{")
                c_out.append(f"{TAB * 2}memcpy(buf, &(message->{field['identifier']}), sizeof(message->{field['identifier']}));")
                c_out.append(f"{TAB}}}")

        c_out.append(f"{TAB}return true;")
        c_out.append("}")

        c_file = open(f"message_{message['name']}.c", 'w')
        c_file.write('\n'.join(c_out))
        c_file.close()

