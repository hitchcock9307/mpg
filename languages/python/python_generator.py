TYPE_MAP = {
    "uint8": "B",
    "uint16": "H",
    "uint32": "I",
    "uint64": "Q",
    "int8": "b",
    "int16": "h",
    "int32": "i",
    "int64": "q",
}

TAB = "    "

class PythonGenerator():
    def __init__(self):
        pass

    def generate_message(self, message):
        out = ["from struct import unpack\n"]
        format_str = ""
        out.extend(f"class {message['name']}:\n")
        for field in message['fields']:
            if field['type'] == 'array':
                if(isinstance(field['length']['expr'], int)):
                    format_str += str(field['length']['expr']) + TYPE_MAP[field['array_type']]
                elif isinstance(field['length']['expr'], str):
                    if(field['length']['expr'] in self.defines):
                        format_str += str(self.defines[field['length']['expr']]) + TYPE_MAP[field['array_type']]
                out.extend([
                    TAB + TAB + "self." + field['identifier'] + " = []\n"
                ])
            else:
                format_str += TYPE_MAP[field['type']]
                out.extend([
                    TAB + TAB + "self." + field['identifier'] + " = 0\n"
                ])

        out.extend(["\n"])
        ## Create deserialize function
        out.extend([
            "def deserialize_" + message['name'] + "(b):\n",
            TAB + "msg = " + message['name'] + "()\n\n",
        ])
        for field in message['fields'][:-1]:
            out.extend([
                TAB + "msg." + field['identifier'] + ",\n",
            ])

        out.extend([
            TAB + "msg." + message['fields'][-1]['identifier'],
            " = unpack('" + format_str + "', b)\n\n",
            TAB + "return msg\n"
        ])
        
        return out

    def _handle_fixed_size(self, param):
        pass