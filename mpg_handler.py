import sys

from languages.python.python_generator import PythonGenerator
from languages.gcc_c.c_generator import CGenerator

def print_err(err):
    print(err)
    exit(1)

generators = {
    "python": ("languages/python", PythonGenerator()),
    "c": ("languages/gcc_c", CGenerator())
}


class MPGHandler():
    def __init__(self, pdl, lang):
        print(pdl)
        generators[lang][1].generate_message(pdl[0])

